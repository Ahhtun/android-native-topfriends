package li.ues.topfriends

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.Spanned
import android.view.Menu
import android.view.View
import android.view.animation.AnimationUtils
import android.webkit.CookieSyncManager
import android.webkit.JavascriptInterface
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.crashlytics.android.Crashlytics
import com.google.android.gms.ads.*
import li.ues.topfriends.utils.CrawUserInfo
import li.ues.topfriends.utils.UserData


/* An instance of this class will be registered as a JavaScript interface */
internal interface RenderJavaScriptInterface {
    @JavascriptInterface
    fun done(width: String, height: String, a: String)
}

internal open class AsyncVerificationOfUserLoggedInOnFacebook : AsyncTask<String, Int, UserData?>() {

    override fun doInBackground(vararg params: String): UserData? {
        publishProgress(1)

        return Utils.getUrl("https://web.facebook.com/", 10).let {
            if (it != null) {
                publishProgress(2)

                Utils.isUserOnHtml(it)
            } else {
                null
            }
        }
    }
}


class MainActivity : GenericFragmentActivity() {
    internal var STATE_OK: Int? = 3
    internal var STATE_REGISTER: Int? = 2
    internal var STATE_LOADING: Int? = 1

    private var mInterstitialAd: InterstitialAd? = null

    private fun fsm(state: Int?, message: String? = null) {
        if (this.isFinishing) {
            return
        }
        val loadingLayout = findViewById<View>(R.id.home_frame_loading) as LinearLayout
        val loadingMessage = findViewById<View>(R.id.home_loading_message) as TextView
        val registerLayout = findViewById<View>(R.id.home_frame_register) as FrameLayout
        val registerMessage = findViewById<View>(R.id.home_register_message) as TextView
        val okLayout = findViewById<View>(R.id.home_frame_ok) as FrameLayout
        val okMessage = findViewById<View>(R.id.home_ok_message) as TextView

        okLayout.visibility = View.GONE
        registerLayout.visibility = View.GONE
        loadingLayout.visibility = View.GONE

        val text: Spanned
        if (message != null) {
            text = Html.fromHtml(message)
        } else {
            text = Html.fromHtml("")
        }

        when (state) {
            3 -> {
                val homeUserName = findViewById<View>(R.id.home_ok_user_name) as TextView
                val homeUserImage = findViewById<View>(R.id.home_ok_user_image) as ImageView
                if (userData != null && userData!!.exists()!!) {
                    homeUserName.text = userData!!.name
                    //homeUserName.setText("Estagiário");
                    try {
                        Glide.with(this)
                            .load(userData!!.profilePicture)
                            .fitCenter()
                            .apply(RequestOptions.circleCropTransform())
                            .placeholder(R.drawable.logo)
                            .into(homeUserImage)
                    } catch (ex: Exception) {
                        logError("Carregar imagem de perfil")
                        ex.printStackTrace()
                    }

                }
                okMessage.text = text
                okLayout.visibility = View.VISIBLE
            }
            2 -> {
                registerMessage.text = text
                registerLayout.visibility = View.VISIBLE
            }
            else -> {
                loadingMessage.text = text
                loadingLayout.visibility = View.VISIBLE
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        println(Global.current)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val loadingLogo = findViewById<View>(R.id.home_loading_image) as ImageView
        loadingLogo.startAnimation(AnimationUtils.loadAnimation(this, R.anim.newanim));

        logAction("Aplicativo acessado")

        MobileAds.initialize(this, "ca-app-pub-1872221771266172~6013176690")

        val mAdView = findViewById<View>(R.id.adView) as AdView
        if (BuildConfig.DEBUG) {
            val adRequest = AdRequest.Builder().addTestDevice("8D246CE1209AEC041682E49E2C87AB56").build()
            mAdView.loadAd(adRequest)
        } else {
            val adRequest = AdRequest.Builder().build()
            mAdView.loadAd(adRequest)
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            CookieSyncManager.createInstance(this);
        }

        val sharedPref = this.getPreferences(Context.MODE_PRIVATE)

        val loggedUser = sharedPref.getString("LOGGED_USER", null)

        if (loggedUser != null) {
            val task = object : AsyncVerificationOfUserLoggedInOnFacebook() {
                override fun onPostExecute(data: UserData?) {
                    if (data == null) {
                        runOnUiThread { fsm(STATE_REGISTER) }
                        return
                    }

                    if (data!!.isOk) {
                        val crawTask = object : CrawUserInfo() {
                            override fun onPostExecute(availableData: UserData?) {
                                runOnUiThread {
                                    if (availableData == null) {
                                        fsm(STATE_REGISTER)
                                    } else {
                                        if (availableData.exists()) {
                                            userData = availableData
                                            Crashlytics.setUserIdentifier(userData!!.id)
                                            Crashlytics.setUserName(userData!!.alias)
                                            fsm(STATE_OK)
                                        } else {
                                            fsm(STATE_REGISTER)
                                        }
                                    }
                                }
                            }
                        }
                        crawTask.execute(data)
                    } else {
                        runOnUiThread { fsm(STATE_REGISTER) }
                    }
                }

                override fun onProgressUpdate(vararg values: Int?) {
                    runOnUiThread {
                        val state = values[0]
                        if (state == 1) {
                            fsm(STATE_LOADING, getString(R.string.main_acessando_facebook))
                        } else if (state == 2 || state == 3) {
                            fsm(STATE_LOADING, getString(R.string.main_procurando_voce))
                        } else {
                            fsm(STATE_LOADING, getString(R.string.main_erro_inesperado))
                        }
                    }
                    super.onProgressUpdate(*values)
                }
            }

            task.execute()

            fsm(STATE_LOADING)
        } else {
            fsm(STATE_REGISTER)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == -0x4e94ff4b) {
            val sharedPref = this.getPreferences(Context.MODE_PRIVATE)
            sharedPref.edit().putString("LOGGED_USER", data!!.toUri(Intent.URI_INTENT_SCHEME)).apply()
            userData = UserData(data)
            val crawTask = object : CrawUserInfo() {
                override fun onPostExecute(availableData: UserData?) {
                    runOnUiThread {
                        if (availableData == null) {
                            fsm(STATE_REGISTER)
                        } else {
                            if (availableData.exists()) {
                                userData = availableData
                                Crashlytics.setUserIdentifier(userData!!.id)
                                Crashlytics.setUserName(userData!!.alias)
                                fsm(STATE_OK)
                            } else {
                                fsm(STATE_REGISTER)
                            }
                        }
                    }
                }
            }
            crawTask.execute(userData)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the m    enu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    fun entrarNoFacebook(view: View) {
        val intent = Intent(this, EntrarNoFacebookActivity::class.java)
        startActivityForResult(intent, 10)
    }

    fun sairDoFacebook(view: View) {
        fsm(STATE_LOADING, getString(R.string.home_saindo_do_facebook))

        Thread {
            try {
                Utils.getUrl("https://m.facebook.com/logout.php?h=AfdTdWsEvfVTAr7L&t=1561522141&source=mtouch_logout_button&persist_locale=1&button_name=logout&button_location=settings")
            } catch (ex: java.lang.Exception) {
                ex.printStackTrace()
            }

            runOnUiThread {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Global.current.cookies.removeAllCookies { }
                } else {
                    CookieSyncManager.createInstance(this@MainActivity)
                }

                userData = null

                fsm(STATE_REGISTER)
            }
        }.start()


//        val intent = Intent(this, EntrarNoFacebookActivity::class.java)
//        startActivityForResult(intent, 10)
    }

    fun preImage(view: View) {
        try {
            fsm(STATE_LOADING)

            mInterstitialAd = InterstitialAd(this)
            mInterstitialAd!!.adUnitId = "ca-app-pub-1872221771266172/4452456229"
            mInterstitialAd!!.loadAd(AdRequest.Builder().addTestDevice("8D246CE1209AEC041682E49E2C87AB56").build())
            mInterstitialAd!!.adListener = object : AdListener() {
                override fun onAdClosed() {
                    super.onAdClosed()

                    fsm(STATE_OK)

                    val intent = Intent(this@MainActivity, PreImageActivity::class.java)
                    startActivity(intent)
                }

                override fun onAdLoaded() {
                    super.onAdLoaded()

                    fsm(STATE_OK)

                    mInterstitialAd!!.show()
                }

                override fun onAdFailedToLoad(i: Int) {
                    super.onAdFailedToLoad(i)

                    fsm(STATE_OK)

                    val intent = Intent(this@MainActivity, PreImageActivity::class.java)
                    startActivity(intent)
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            fsm(STATE_OK)

            val intent = Intent(this@MainActivity, PreImageActivity::class.java)
            startActivity(intent)
        }

    }

    companion object {
        var userData: UserData? = null
    }
}
